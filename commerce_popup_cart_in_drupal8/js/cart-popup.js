(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.cartpopup_behavior = {
  attach: function(context, settings) {
    
    // Shopping cart block contents
    if ( $.isFunction($.fn.hoverIntent) ) {            
      $("#block-commerce-popup-cart-commerce-popup-cart").hoverIntent({
          sensitivity: 7, // number = sensitivity threshold (must be 1 or higher)
          interval: 50,   // number = milliseconds of polling interval
          over: function () {          
              
                var wrapper = $(this).find('#cart-popup');
                wrapper.show();
              
        },  // function = onMouseOver callback (required)
          timeout: 500,   // number = milliseconds delay before onMouseOut function call
          out: function () {
             $('#cart-popup').hide();
        }    // function = onMouseOut callback (required)
      });
    }           
    

  }
};


})(jQuery, Drupal, this, this.document);
