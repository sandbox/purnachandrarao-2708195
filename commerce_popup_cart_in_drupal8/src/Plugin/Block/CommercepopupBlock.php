<?php

/**
 * @file
 * Contains \Drupal\commerce_popup_cart\Plugin\Block\CommercepopupBlock.
 */

namespace Drupal\commerce_popup_cart\Plugin\Block;

use Drupal\block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\LineItem;
use Drupal\commerce_order\Entity\LineItemType;
use Drupal\profile\Entity\Profile;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Provides a 'Commerce Popup Cart Information' block.
 *
 * @Block(
 *   id = "commerce_popup_block",
 *   admin_label = @Translation("Commerce Popup Cart Information")
 * )
 */
class CommercepopupBlock extends BlockBase{
  
  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access cric information');
  }
  /**
   * {@inheritdoc}
   */

  public function blockValidate($form, FormStateInterface $form_state) {
    /*if (!is_numeric($form_state['values']['count'])) {
      form_set_error('count', $form_state, t('Count must be numeric'));
    }*/
  }

  /**
   * {@inheritdoc}
   */
  public function build() {    
  $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
  $user_id= $user->get('uid')->value;
  $order = \Drupal::entityManager()->getStorage('commerce_order')->load($user_id);
foreach ($order->getLineItems() as $line_item) {
    $total_price = $line_item->total_price->amount;
    $quantity= $line_item->get('quantity')->value;
    $title= $line_item->get('title')->value;
	
	if ($quantity > 0){
            $icon = '<div class="cart-icon"></div><span class="cart_popup_count">'.$quantity.'</span>';
            $content = '<div id="cart-popup" style="display:none;">'.$title.'<div class="popup-arrow"></div></div>';
            $content = '<div class="wrapper">' . $icon . $content . $total_price.'</div>';  
          }else{
            $icon = '<div class="cart-icon"></div><span class="cart_popup_count"></span>';
            $content = '<div id="cart-popup" style="display:none;">commerce_popup_cart_empty_cart_msg<div class="popup-arrow"></div></div>';
            $content = '<div class="wrapper">' . $icon . $content .$total_price. '</div>';
          }
   }
  
  return array(
      '#type' => 'markup',
      '#markup' => $content,
	  '#attached' => array(
        'library' =>  array(
          'commerce_popup_cart/block-preview'
        ),
      ),
    );
  }
  
  
}
